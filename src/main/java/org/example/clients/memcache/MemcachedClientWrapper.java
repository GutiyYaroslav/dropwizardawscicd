package org.example.clients.memcache;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.MemcachedClient;

import java.io.IOException;

public class MemcachedClientWrapper {

    private final MemcachedClient memcachedClient;

    public MemcachedClientWrapper() throws IOException {
        memcachedClient = new MemcachedClient(
                AddrUtil.getAddresses("testmemcached.yvo2qk.cfg.euc1.cache.amazonaws.com:11211"));
    }

    public void set(String key, int expiration, Object value) {
        memcachedClient.set(key, expiration, value);
    }

    public Object get(String key) {
        return memcachedClient.get(key);
    }

    public void close() {
        memcachedClient.shutdown();
    }

    //TODO: add address to config
}
