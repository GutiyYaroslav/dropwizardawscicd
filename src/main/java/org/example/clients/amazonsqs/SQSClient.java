package org.example.clients.amazonsqs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.example.enums.SQSQueueType;
import org.example.models.egg.UserEgg;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;

public class SQSClient {
    private final ObjectMapper objectMapper;
    private final SqsClient sqsClient;

    public SQSClient() {
        this.objectMapper = new ObjectMapper();
        sqsClient = createSqsClient(createAwsCredentialProvider());
    }

    public void sendMessageToQueue(UserEgg userEgg, SQSQueueType sqsQueueType){
        try {
            String messageBody = objectMapper.writeValueAsString(userEgg);
            SendMessageRequest request = SendMessageRequest.builder()
                    .queueUrl(sqsQueueType.getQueueUrl())
                    .messageBody(messageBody)
                    .build();
            sqsClient.sendMessage(request);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private SqsClient createSqsClient(AwsCredentialsProvider awsCredentialsProvider) {
        return SqsClient.builder()
                .region(Region.EU_CENTRAL_1)
                .credentialsProvider(awsCredentialsProvider)
                .build();
    }

    private AwsCredentialsProvider createAwsCredentialProvider() {
        return () -> AwsBasicCredentials.create("AKIA33P7LRWGTYZ2QXFV", "MGL3pe5cMpjveBqB10RvxpgAOFAmqBvzmUuQqvXk");
    }
}
