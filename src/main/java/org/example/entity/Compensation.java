package org.example.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Compensation {
    private int salary;
    private int bonuses;

    public Compensation(int salary, int bonuses) {
        this.salary = salary;
        this.bonuses = bonuses;
    }

    @JsonProperty
    public int getSalary() {
        return salary;
    }

    @JsonProperty
    public void setSalary(int salary) {
        this.salary = salary;
    }

    @JsonProperty
    public int getBonuses() {
        return bonuses;
    }

    @JsonProperty
    public void setBonuses(int bonuses) {
        this.bonuses = bonuses;
    }
}
