package org.example.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.example.enums.Role;

import java.io.Serializable;


public class User  implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private Role role;
    private int experience;
    private String email;
    private String password;

    public User() {
    }

    public User(Long id, String firstName, String lastName, Role role, int experience, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.experience = experience;
        this.email = email;
        this.password = password;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonProperty
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty
    public String getLastName() {
        return lastName;
    }

    @JsonProperty
    public Role getRole() {
        return role;
    }

    @JsonProperty
    public int getExperience() {
        return experience;
    }

    @JsonProperty
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty
    public void setRole(Role role) {
        this.role = role;
    }

    @JsonProperty
    public void setExperience(int experience) {
        this.experience = experience;
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    @JsonProperty
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public static class Builder {
        private Long id;
        private String firstName;
        private String lastName;
        private Role role;
        private int experience;
        private String email;
        private String password;

        public Builder() {
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withRole(Role role) {
            this.role = role;
            return this;
        }

        public Builder withExperience(int experience) {
            this.experience = experience;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public User build() {
            return new User(id, firstName, lastName, role, experience, email, password);
        }
    }

}
