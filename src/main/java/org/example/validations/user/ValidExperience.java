package org.example.validations.user;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;

import java.lang.annotation.*;

//@Documented
@Constraint(validatedBy = {})
@NotNull(message = "Experience must not be null")
@Max(value = 50, message = "Experience must be less than or equal to 50")
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidExperience {
    String message() default "Invalid experience";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
