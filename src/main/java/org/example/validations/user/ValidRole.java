package org.example.validations.user;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.constraints.NotNull;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//@Documented
@Constraint(validatedBy = {ValidRoleValidator.class})
@NotNull(message = "Role must not be null")
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidRole {
    String message() default "Invalid role: should be BOSS, MANAGER or EMPLOYEE";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
