package org.example.validations.user;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import java.lang.annotation.*;

//@Documented
@Constraint(validatedBy = {})
@NotNull(message = "Password must not be null")
@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=]).+$",
        message = "Invalid password format")
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPassword {
    String message() default "Invalid password: Password must contain at least one " +
            "uppercase letter, at least one lowercase letter, at least one digit," +
            " and at least one special character";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
