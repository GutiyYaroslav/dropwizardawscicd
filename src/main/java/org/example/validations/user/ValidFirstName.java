package org.example.validations.user;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.ReportAsSingleViolation;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint(validatedBy = {})
@NotBlank(message = "First name must not be empty")
@Pattern(regexp = "^[a-zA-Z]+$", message = "First name must contain only letters")
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
public @interface ValidFirstName {
    String message() default "Invalid first name";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
