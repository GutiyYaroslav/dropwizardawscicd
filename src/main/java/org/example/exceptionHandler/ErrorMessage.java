package org.example.exceptionHandler;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorMessage {
    private String errorMessage;

    public ErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonProperty
    public String getErrorMessage() {
        return errorMessage;
    }

    @JsonProperty
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
