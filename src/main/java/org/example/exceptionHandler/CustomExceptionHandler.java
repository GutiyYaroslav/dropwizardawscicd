package org.example.exceptionHandler;

import io.dropwizard.jersey.errors.ErrorMessage;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.example.exceptions.UserEmailAlreadyExistsException;
import org.example.exceptions.UserNotFoundException;

@Provider
public class CustomExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {
        int statusCode;
        ErrorMessage errorMessage;

        if (exception instanceof UserNotFoundException) {
            statusCode = 404;
            errorMessage = new ErrorMessage(404, "User with such id not found");
        } else if (exception instanceof UserEmailAlreadyExistsException) {
            statusCode = 409;
            errorMessage = new ErrorMessage(409, "User email already exists");
        } else {
            statusCode = 500;
            errorMessage = new ErrorMessage(500, "Internal Server Error");
        }
        return Response.status(statusCode)
                .entity(errorMessage)
                .build();
    }
    //TODO: create similar exception responses
}