package org.example.enums;

public enum SQSQueueType {
    EMAIL_QUEUE("https://sqs.eu-central-1.amazonaws.com/814968704397/emailqueue"),
    REGISTER_QUEUE("https://sqs.eu-central-1.amazonaws.com/814968704397/registerqueue");

    private final String queueUrl;

    SQSQueueType(String queueUrl) {
        this.queueUrl = queueUrl;
    }

    public String getQueueUrl() {
        return queueUrl;
    }
}