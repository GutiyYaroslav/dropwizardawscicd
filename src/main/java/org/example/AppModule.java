package org.example;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import io.dropwizard.core.setup.Environment;
import io.dropwizard.jdbi3.JdbiFactory;
import org.example.clients.amazonsqs.SQSClient;
import org.example.clients.memcache.MemcachedClientWrapper;
import org.example.configurations.AppConfig;
import org.example.dao.UserDAO;
import org.example.dataStore.UserDataStore;
import org.example.entity.User;
import org.example.resourses.CacheResource;
import org.example.resourses.UserResource;
import org.example.services.UserService;
import org.jdbi.v3.core.Jdbi;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AppModule extends DropwizardAwareModule<AppConfig> {
    private static final String DB_NAME = "mysql";

    @Override
    protected void configure() {
        Jdbi jdbi = getJdbi();
        bind(UserDAO.class).toInstance(jdbi.onDemand(UserDAO.class));
        bind(UserDataStore.class);
        bind(UserService.class);
        bind(UserResource.class);
        bind(CacheResource.class);
    }

    @Provides
    @Singleton
    public Cache<String, List<User>> employeeCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(3, TimeUnit.MINUTES)
                .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                .build();
    }

    @Provides
    @Singleton
    public MemcachedClientWrapper getMemcachedClientWrapper() throws IOException {
        return new MemcachedClientWrapper();
    }

    @Provides
    @Singleton
    public SQSClient getSQSClient() {
        return new SQSClient();
    }

    private Jdbi getJdbi() {
        AppConfig appConfig = configuration();
        Environment environment = environment();
        JdbiFactory factory = new JdbiFactory();
        return factory.build(environment, appConfig.getDataSourceFactory(), DB_NAME);
    }
}