package org.example.compensation;

import org.example.entity.Compensation;
import org.example.entity.User;

public abstract class CompensationCalculator {
    protected User user;
    public CompensationCalculator(User user) {
        this.user = user;
    }

    public abstract Compensation calculate();
}
