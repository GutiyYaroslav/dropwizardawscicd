package org.example.compensation;

import org.example.entity.User;
import org.example.enums.Role;

public class CalculatorFactory {
    public static CompensationCalculator getCalculator(User user){
        if(user.getRole().equals(Role.MANAGER)){
            return new ManagerCompensationCalculator(user);
        }else if(user.getRole().equals(Role.EMPLOYEE)){
            return new EmployeeCompensationCalculator(user);
        }else if(user.getRole().equals(Role.BOSS)){
            return new BossCompensationCalculator(user);
        }else{
            throw new IllegalArgumentException("User is not valid: - " + user );
        }
    }
}
