package org.example.compensation;

import org.example.entity.Compensation;
import org.example.entity.User;

public class ManagerCompensationCalculator extends CompensationCalculator{


    public ManagerCompensationCalculator(User user) {
        super(user);
    }

    @Override
    public Compensation calculate() {
        int basicSalary = 100 * user.getExperience();
        int fullSalary = (int) (basicSalary * 0.07 + basicSalary);
        return new Compensation(fullSalary , 7);
    }
}
