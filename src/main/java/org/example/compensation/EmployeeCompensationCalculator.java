package org.example.compensation;

import org.example.entity.Compensation;
import org.example.entity.User;

public class EmployeeCompensationCalculator extends CompensationCalculator{

    public EmployeeCompensationCalculator(User user) {
        super(user);
    }

    @Override
    public Compensation calculate() {
        return new Compensation(100*user.getExperience(), 0);
    }
}
