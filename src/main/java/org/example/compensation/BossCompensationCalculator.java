package org.example.compensation;

import org.example.entity.Compensation;
import org.example.entity.User;

public class BossCompensationCalculator extends CompensationCalculator{

    public BossCompensationCalculator(User user) {
        super(user);
    }

    @Override
    public Compensation calculate() {
        return new Compensation(1000, 100);
    }
}
