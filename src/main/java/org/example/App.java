package org.example;


import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import io.dropwizard.jersey.validation.HibernateValidationBinder;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.example.configurations.AppConfig;
import org.example.exceptionHandler.CustomExceptionHandler;
import ru.vyarus.dropwizard.guice.GuiceBundle;



public class App extends Application<AppConfig> {

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public String getName() {
        return "my-application-users-rest";
    }

    @Override
    public void initialize(Bootstrap<AppConfig> bootstrap) {
        bootstrap.addBundle(GuiceBundle.builder()
                .modules(new AppModule())
                .build());
    }

    @Override
    public void run(AppConfig appConfig, Environment environment){
//        JdbiFactory factory = new JdbiFactory();
//        Jdbi jdbi = factory.build(environment, appConfig.getDataSourceFactory(), "dbname");
//        UserResource resource = new UserResource(new UserService(jdbi.onDemand(UserDAO.class)));
//        environment.jersey().register(resource);
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        environment.jersey().register(new HibernateValidationBinder(validator));

        environment.jersey().register(new CustomExceptionHandler());
    }

}
