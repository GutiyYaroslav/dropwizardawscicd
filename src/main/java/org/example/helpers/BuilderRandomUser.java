package org.example.helpers;

import org.example.entity.User;
import org.example.enums.Role;
import org.example.models.egg.UserEgg;

import java.util.Random;

public class BuilderRandomUser {
    private static final String ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 20;
    private static final String ALLOWED_CHARACTERS_PASSWORD = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-_=+";
    private static final int PASSWORD_LENGTH = 8;
    private static final int MIN_EXPERIENCE = 0;
    private static final int MAX_EXPERIENCE = 30;
    private static Random random = new Random();

    public static UserEgg createRandomUserEgg(){
        return new UserEgg.UserEggBuilder()
                .withFirstName(generateRandomFirstName())
                .withLastName(generateRandomLastName())
                .withRole(String.valueOf(generateRandomRole()))
                .withExperience(generateRandomExperience())
                .withEmail(generateRandomEmail())
                .withPassword(generateRandomPassword())
                .build();
    }

    private static String generateRandomFirstName() {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(MAX_NAME_LENGTH - MIN_NAME_LENGTH + 1) + MIN_NAME_LENGTH;

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
            char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    private static String generateRandomLastName() {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(MAX_NAME_LENGTH - MIN_NAME_LENGTH + 1) + MIN_NAME_LENGTH;

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
            char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    private static Role generateRandomRole() {
        Role[] roles = Role.values();
        int randomIndex = random.nextInt(roles.length);
        return roles[randomIndex];
    }

    private static String generateRandomPassword() {
        StringBuilder sb = new StringBuilder();

        // Додати велику літеру
        char randomUppercaseLetter = generateRandomCharacterFromRange('A', 'Z');
        sb.append(randomUppercaseLetter);

        // Додати маленьку літеру
        char randomLowercaseLetter = generateRandomCharacterFromRange('a', 'z');
        sb.append(randomLowercaseLetter);

        // Додати цифру
        char randomDigit = generateRandomCharacterFromRange('0', '9');
        sb.append(randomDigit);

        // Додати символ
        char randomSymbol = generateRandomSymbol();
        sb.append(randomSymbol);

        // Додати решту символів
        for (int i = 0; i < PASSWORD_LENGTH - 4; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARACTERS_PASSWORD.length());
            char randomChar = ALLOWED_CHARACTERS_PASSWORD.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    private static char generateRandomCharacterFromRange(char min, char max) {
        return (char) (random.nextInt(max - min + 1) + min);
    }

    private static char generateRandomSymbol() {
        String symbols = "!@#$%^&*()-_=+";
        int randomIndex = random.nextInt(symbols.length());
        return symbols.charAt(randomIndex);
    }

    private static int generateRandomExperience() {
        return random.nextInt(MAX_EXPERIENCE - MIN_EXPERIENCE + 1) + MIN_EXPERIENCE;
    }

    private static String generateRandomEmail() {
        StringBuilder sb = new StringBuilder();

        // Генерувати випадкову частину перед символом '@'
        String randomPart = generateRandomString(MAX_NAME_LENGTH - 6); // Віднімаємо 6 символів для "@domain.com"
        sb.append(randomPart);
        sb.append("@domain.com");

        return sb.toString();
    }

    private static String generateRandomString(int maxLength) {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(maxLength - MIN_NAME_LENGTH + 1) + MIN_NAME_LENGTH;

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
            char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }
}



