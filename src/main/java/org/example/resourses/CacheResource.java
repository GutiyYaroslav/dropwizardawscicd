package org.example.resourses;


import com.google.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.example.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/cache")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CacheResource {
    private final UserService userService;
    private static Logger logger = LoggerFactory.getLogger(UserResource.class);

    @Inject
    public CacheResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Path("/db/1")
    public Response getAllUsersFromDb1(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromDb();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute query to database (cold) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }

    @GET
    @Path("/db/2")
    public Response getAllUsersFromDb2(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromDb();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute query to database (warm) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }

    @GET
    @Path("/local/1")
    public Response getAllUsersFromLocalCache1(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromLocalCache();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute call to local cache (cold) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }

    @GET
    @Path("/local/2")
    public Response getAllUsersFromLocalCache2(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromLocalCache();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute call to local cache (warm) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }

    @GET
    @Path("/distributed/1")
    public Response getAllUsersFromDistributedCache1(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromDistributedCache();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute call to distributed cache (cold) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }

    @GET
    @Path("/distributed/2")
    public Response getAllUsersFromDistributedCache2(){
        long startTime = System.currentTimeMillis();
        Object users = userService.getAllUsersFromDistributedCache();
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        logger.info(" ----- CUSTOM LOGGER ---- | Time execute call to distributed cache (warm) --- " + responseTime + " ms");
        return Response.ok(users).build();
    }
}
