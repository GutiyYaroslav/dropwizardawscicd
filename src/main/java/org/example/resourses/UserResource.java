package org.example.resourses;


import com.google.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.example.entity.User;
import org.example.enums.Role;
import org.example.helpers.BuilderRandomUser;
import org.example.models.egg.UserEgg;
import org.example.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    private final UserService userService;
    private static Logger logger = LoggerFactory.getLogger(UserResource.class);

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") Long userId){
            return Response.ok(userService.getUserById(userId)).build();
    }

    @GET
    public Response getAllUsers() {
        return Response.ok(userService.getAllUsers()).build();
    }

    @POST
    public Response insertUser(@Valid UserEgg userEgg){
        User createdUser = userService.createUser(userEgg);
        return Response.created(URI.create("/users/" + createdUser.getId()))
                .entity(createdUser)
                .build();
    }

    @DELETE
    @Path("/{userId}")
    public Response deleteUserById(@PathParam("userId") Long userId){
        userService.deleteUserById(userId);
        return Response.noContent().build();
    }
//    test queue

    @POST
    @Path("/testqueue")
    public Response testQueueCreateUserAndSendEmail(UserEgg userEgg){
        userService.registrationAndSendMessage(userEgg);
        return Response.accepted().build();
    }

    @GET
    @Path("/register/{count}")
    public Response createRandomUserByCounter(@PathParam("count") int count){
        userService.registrationUsersByCount(count);
        return Response.accepted().build();
    }
//    test create random user
    @GET
    @Path("/testrandom")
    public Response createRandomUser(){
        UserEgg userEgg = BuilderRandomUser.createRandomUserEgg();
        return Response.ok(userEgg).build();
    }

}
