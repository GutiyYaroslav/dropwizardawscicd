package org.example.models.egg;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.example.validations.user.*;

public class UserEgg {
    @ValidFirstName
    private String firstName;
    @ValidLastName
    private String lastName;
    @ValidRole
    private String role;
    @ValidExperience
    private int experience;
    @ValidEmail
    private String email;
    @ValidPassword
    private String password;

    public UserEgg() {
    }

    public UserEgg(String firstName, String lastName, String role, int experience, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.experience = experience;
        this.email = email;
        this.password = password;
    }

    @JsonProperty
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty
    public String getLastName() {
        return lastName;
    }

    @JsonProperty
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty
    public String getRole() {
        return role;
    }

    @JsonProperty
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty
    public int getExperience() {
        return experience;
    }

    @JsonProperty
    public void setExperience(int experience) {
        this.experience = experience;
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    @JsonProperty
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserEgg{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", role='" + role + '\'' +
                ", experience=" + experience +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
    public static class UserEggBuilder {
        private String firstName;
        private String lastName;
        private String role;
        private int experience;
        private String email;
        private String password;

        public UserEggBuilder() {
        }

        public UserEggBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserEggBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserEggBuilder withRole(String role) {
            this.role = role;
            return this;
        }

        public UserEggBuilder withExperience(int experience) {
            this.experience = experience;
            return this;
        }

        public UserEggBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserEggBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserEgg build() {
            return new UserEgg(firstName, lastName, role, experience, email, password);
        }
    }
}
