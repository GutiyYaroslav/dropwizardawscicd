package org.example.dataStore;

import com.google.common.cache.Cache;
import com.google.inject.Inject;
import org.example.clients.memcache.MemcachedClientWrapper;
import org.example.dao.UserDAO;
import org.example.models.egg.UserEgg;
import org.example.entity.User;
import org.example.exceptions.UserEmailAlreadyExistsException;
import org.example.exceptions.UserNotFoundException;

import java.util.List;

public class UserDataStore {
    private final UserDAO userDAO;
    private final Cache<String,List<User>> userCache;
    private final MemcachedClientWrapper memcached;
    private static int counterCall = 0;

    @Inject
    public UserDataStore(UserDAO userDAO, Cache<String, List<User>> userCache, MemcachedClientWrapper memcached) {
        this.userDAO = userDAO;
        this.userCache = userCache;
        this.memcached = memcached;
    }

    public List<User> getAllUsers(){
        List<User> cachedUsers = userCache.getIfPresent("allUsers");
        if (cachedUsers == null) {
            cachedUsers = userDAO.getAllUsers();
            userCache.put("allUsers", cachedUsers);
        }
        return cachedUsers;
    }

    public User getUser(Long userId){
        return userDAO.getUserById(userId).orElseThrow(
                () -> new UserNotFoundException("User with such id was not found"));
    }

    public void deleteUser(Long userId){
        if(!userDAO.deleteUserById(userId)){
            throw new UserNotFoundException("User with such id = " + userId + " was not found");
        }
    }

    public User createUser(UserEgg userEgg) {
        if(userDAO.getUserByEmail(userEgg.getEmail()).isPresent()){
            throw new UserEmailAlreadyExistsException("User with such email - " + userEgg.getEmail() + " already exists");
        }else{
            if(userDAO.insertUser(userEgg)){
                return (userDAO.getUserByEmail(userEgg.getEmail()).orElseThrow());
            }else{
                throw new IllegalArgumentException();
            }
        }
    }

    public User getUser(String userEmail){
        return userDAO.getUserByEmail(userEmail).orElseThrow(
                () -> new UserNotFoundException("User with such email was not found"));
    }

//    test time response
    public Object getAllUsersTest(){
        if(counterCall == 0){
            List<User> users = userDAO.getAllUsers();
            userCache.put("allUsers", users);
            memcached.set("allUsers",10, users);
            counterCall++;
            return users;

        }else if(counterCall == 1){
            counterCall++;
            return userDAO.getAllUsers();

        }else if(counterCall == 2){
            counterCall++;
            return userCache.getIfPresent("allUsers");

        }else if(counterCall == 3){
            counterCall++;
            return memcached.get("allUsers");

        }else{
            return userDAO.getAllUsers();
        }
    }

    public Object getAllUsersFromDb(){
        return userDAO.getAllUsers();
    }

    public Object getAllUsersFromLocalCache(){
        List<User> cachedUsers = userCache.getIfPresent("allUsersNewKey");
        if (cachedUsers == null) {
            cachedUsers = userDAO.getAllUsers();
            userCache.put("allUsersNewKey", cachedUsers);
        }
        return cachedUsers;
    }

    public Object getAllUsersFromDistributedCache(){
        Object cachedUsers = memcached.get("allUsersNewKey");
        if (cachedUsers == null) {
            cachedUsers = userDAO.getAllUsers();
            memcached.set("allUsersNewKey",10, cachedUsers);
        }
        return cachedUsers;
    }
}
