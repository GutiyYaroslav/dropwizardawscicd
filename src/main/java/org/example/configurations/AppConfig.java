package org.example.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.core.Configuration;
import io.dropwizard.db.DataSourceFactory;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;


public class AppConfig extends Configuration {
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("validationEnabled")
    private boolean validationEnabled;

//    @NotEmpty
//    private String secretKey;

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    @JsonProperty("validationEnabled")
    public boolean isValidationEnabled() {
        return validationEnabled;
    }

    @JsonProperty("validationEnabled")
    public void setValidationEnabled(boolean validationEnabled) {
        this.validationEnabled = validationEnabled;
    }

//    @JsonProperty
//    public String getSecretKey() {
//        return secretKey;
//    }
//
//    @JsonProperty
//    public void setSecretKey(String secretKey) {
//        this.secretKey = secretKey;
//    }
}
