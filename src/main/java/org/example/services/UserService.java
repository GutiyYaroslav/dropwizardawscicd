package org.example.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.example.clients.amazonsqs.SQSClient;
import org.example.dataStore.UserDataStore;
import org.example.enums.SQSQueueType;
import org.example.helpers.BuilderRandomUser;
import org.example.models.egg.UserEgg;
import org.example.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;

import java.util.List;

public class UserService{
    private final UserDataStore userDataStore;
    private final SQSClient sqsClient;
    private final ObjectMapper objectMapper;
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);


    @Inject
    public UserService(UserDataStore userDataStore, SQSClient sqsClient) {
        this.userDataStore = userDataStore;
        this.sqsClient = sqsClient;
        this.objectMapper = new ObjectMapper();
    }

    public List<User> getAllUsers() {
            return userDataStore.getAllUsers();
    }

    public User getUserById(Long userId) {
        return userDataStore.getUser(userId);
    }

    public User createUser(UserEgg userEgg) {
        return userDataStore.createUser(userEgg);
    }

    public void deleteUserById(Long userId) {
        userDataStore.deleteUser(userId);
    }

    public User getUserByEmail(String userEmail){
        return userDataStore.getUser(userEmail);
    }

//    public Compensation getUserCompensationByUserId(Long id) {
//        return CalculatorFactory.getCalculator(getUserById(id)).calculate();
//    }


//    test time cache response
    public Object getAllUsersTest(){
        return userDataStore.getAllUsersTest();
    }

    public Object getAllUsersFromDb(){
        return userDataStore.getAllUsersFromDb();
    }

    public Object getAllUsersFromLocalCache(){
        return userDataStore.getAllUsersFromLocalCache();
    }

    public Object getAllUsersFromDistributedCache(){
        return userDataStore.getAllUsersFromDistributedCache();
    }

//    test queue
    public void registrationAndSendMessage(UserEgg userEgg){
        sqsClient.sendMessageToQueue(userEgg, SQSQueueType.EMAIL_QUEUE);
        sqsClient.sendMessageToQueue(userEgg, SQSQueueType.REGISTER_QUEUE);
        logger.info("SERVICE - SEND DATA TO QUEUE");

    }

    public void registrationUsersByCount(int count){
        for(int i = 0; i < count; i++){
            UserEgg userEgg = BuilderRandomUser.createRandomUserEgg();
            sqsClient.sendMessageToQueue(userEgg, SQSQueueType.REGISTER_QUEUE);
            logger.info("SERVICE - SEND DATA TO QUEUE");
        }
    }
}
