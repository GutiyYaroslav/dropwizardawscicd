package org.example.dao;

import org.example.models.egg.UserEgg;
import org.example.entity.User;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;


import java.util.List;
import java.util.Optional;

@RegisterBeanMapper(User.class)
public interface UserDAO {
    @SqlQuery("SELECT * FROM users WHERE id = :id LIMIT 1")
    Optional<User> getUserById(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM users")
    List<User> getAllUsers();

    @SqlUpdate("insert into users (firstName, lastName, role, experience, email, password) " +
               "values (:firstName, :lastName, :role, :experience, :email, :password)")
    boolean insertUser(@BindBean UserEgg userEgg);
    //TODO: egg/immutable

    @SqlUpdate("delete from users where id = :id")
    boolean  deleteUserById(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM users WHERE email = :email LIMIT 1")
    Optional<User> getUserByEmail(@Bind("email") String email);

//    @SqlQuery("SELECT * FROM users WHERE firstName = :firstName AND lastName = :lastName " +
//            "AND role = :role AND experience = :experience")
//    User getUserByAllParams(@BindBean User user);
}
